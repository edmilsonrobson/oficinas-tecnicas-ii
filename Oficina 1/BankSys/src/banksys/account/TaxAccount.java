package banksys.account;

import banksys.account.exception.InsufficientFundsException;
import banksys.account.exception.NegativeAmountException;

public class TaxAccount extends AbstractAccount {

	public TaxAccount(String number) {
		super(number);
	}

	public void debit(double amount) throws NegativeAmountException, InsufficientFundsException {
		if (amount > 0) {			
			if (this.balance > getAmountWithTaxes(amount)) {
				this.balance = this.balance - getAmountWithTaxes(amount);
			} else {
				throw new InsufficientFundsException(number, amount);
			}
		} else {
			throw new NegativeAmountException(amount);
		}
	}

	private double getAmountWithTaxes(double amount) {
		return amount + (amount * 0.001);
	}
}
