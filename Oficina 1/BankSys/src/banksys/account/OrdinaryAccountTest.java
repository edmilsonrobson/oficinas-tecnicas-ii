package banksys.account;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import banksys.account.exception.InsufficientFundsException;
import banksys.account.exception.NegativeAmountException;

public class OrdinaryAccountTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDebit() {
		OrdinaryAccount account = new OrdinaryAccount("123A");
		try{
			account.debit(50);
		}catch(NegativeAmountException e){
			fail(e.getMessage());
		} catch(InsufficientFundsException e){
			fail(e.getMessage());		
		}
		assertEquals("Ordinary Account debit test fail!",50, account.getBalance());
	}

	@Test (expected = NegativeAmountException.class)
	public void testDebitWithNegative() throws NegativeAmountException {
		OrdinaryAccount account = new OrdinaryAccount("123A");
		try{
			account.debit(-10);
		}catch ( InsufficientFundsException e){
			fail(e.getMessage());
		}
	}

	@Test (expected = InsufficientFundsException.class)
	public void testDebitWithInsuffientFunds() throws InsufficientFundsException, NegativeAmountException {
		OrdinaryAccount account = new OrdinaryAccount("123A");
		try{
			account.debit(-10);
		}catch ( InsufficientFundsException e){
			fail(e.getMessage());
		}
	}

	@Test
	public void testCredit() {
		fail("Not yet implemented");
	}

}
