package banksys.account;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import banksys.account.exception.InsufficientFundsException;
import banksys.account.exception.NegativeAmountException;

public class TaxAccountTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testCredit() {
		TaxAccount account = new TaxAccount("123B");
		try{
			account.debit(50);
		}catch(NegativeAmountException e){
			fail(e.getMessage());
		} catch(InsufficientFundsException e){
			fail(e.getMessage());		
		}
		assertEquals("Ordinary Account debit test fail!",50, account.getBalance());
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testDebit() {
		TaxAccount account = new TaxAccount("123B");
		try{
			account.debit(30);
		}catch(NegativeAmountException e){
			fail(e.getMessage());
		} catch(InsufficientFundsException e){
			fail(e.getMessage());		
		}
		assertEquals("Ordinary Account debit test fail!",30, account.getBalance());
	}
	
	@Test (expected = NegativeAmountException.class)
	public void testDebitWithNegative() throws NegativeAmountException {
		TaxAccount account = new TaxAccount("123B");
		try{
			account.debit(-10);
		}catch ( InsufficientFundsException e){
			fail(e.getMessage());
		}
	}

	@Test (expected = InsufficientFundsException.class)
	public void testDebitWithInsuffientFunds() throws InsufficientFundsException, NegativeAmountException {
		TaxAccount account = new TaxAccount("123B");
		try{
			account.debit(-10);
		}catch ( InsufficientFundsException e){
			fail(e.getMessage());
		}
	}
	
	@Test (expected = Exception.class)
	public void testEmptyAccountName(){
		try{
			TaxAccount account = new TaxAccount("");
		}catch(Exception e){
			fail(e.getMessage());
		}
	}




}
