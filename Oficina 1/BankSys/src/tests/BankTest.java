package tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.internal.runners.statements.Fail;

import banksys.account.AbstractAccount;
import banksys.account.OrdinaryAccount;
import banksys.account.TaxAccount;
import banksys.account.exception.InsufficientFundsException;
import banksys.account.exception.NegativeAmountException;
import banksys.control.BankController;
import banksys.control.exception.BankTransactionException;
import banksys.persistence.AccountVector;

import org.junit.Assert;

public class BankTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void ordinaryAccountTest() {
		AbstractAccount account = new OrdinaryAccount("123A");
		try {
			account.credit(50);
		} catch (NegativeAmountException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertEquals(50.0, account.getBalance(), 0.0001);

		try {
			account.debit(30);
		} catch (NegativeAmountException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InsufficientFundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertEquals(20.0, account.getBalance(), 0.0001);

	}

	@Test
	public void taxAccountTest() {
		AbstractAccount account = new TaxAccount("123B");
		try {
			account.credit(50);
		} catch (NegativeAmountException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertEquals(50.0, account.getBalance(), 0.0001);

		try {
			account.debit(30);
		} catch (NegativeAmountException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InsufficientFundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertEquals(20.1697, account.getBalance() * 1.01, 0.0001);

	}

	@Test
	public void savingsAccountTest() {
		AbstractAccount account = new OrdinaryAccount("123C");
		try {
			account.credit(50);
		} catch (NegativeAmountException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertEquals(50.0, account.getBalance(), 0.0001);

		try {
			account.debit(30);
		} catch (NegativeAmountException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InsufficientFundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertEquals(20.0, account.getBalance(), 0.0001);

	}

	@Test
	public void specialAccountTest() {
		AbstractAccount account = new OrdinaryAccount("123B");
		try {
			account.credit(50);
		} catch (NegativeAmountException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertEquals(50.0, account.getBalance(), 0.0001);

		try {
			account.debit(30);
		} catch (NegativeAmountException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InsufficientFundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Assert.assertEquals(20.0, account.getBalance(), 0.0001);

	}

	@Test
	public void transferBetweenAccountsTest() throws NegativeAmountException, BankTransactionException {
		AbstractAccount accountA = new OrdinaryAccount("123A");
		AbstractAccount accountB = new OrdinaryAccount("123C");
		accountA.credit(50);

		BankController controller = new BankController(new AccountVector());

		controller.addAccount(accountA);
		controller.addAccount(accountB);
		controller.doTransfer("123A", "123C", 10.0);

		Assert.assertEquals(40.0, accountA.getBalance(), 0.0001);
		Assert.assertEquals(10.0, accountB.getBalance(), 0.0001);

	}

	@Test(expected = BankTransactionException.class)
	public void addTwoAccountsSameNumber() throws BankTransactionException {
		AbstractAccount accountA = new OrdinaryAccount("123A");
		AbstractAccount accountB = new OrdinaryAccount("123A");

		BankController controller = new BankController(new AccountVector());

		controller.addAccount(accountA);
		controller.addAccount(accountB);
	}

}
